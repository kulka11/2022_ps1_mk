/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package javaapp;
import java.util.Random;


public class JavaApp {


    public static void main(String[] args) {
        final int N=10;
        int tab[] = new int[N];
        
        for(int i=0;i<N;i++)
            tab[ i ] = new Random().nextInt();
        
        int min = tab[0];
        int max = tab[0];
        for(int i=0;i<N;i++){
            if(min>tab[i])min=tab[i];
            if(max<tab[i])max=tab[i];
        }
        System.out.println("min="+min+", max="+max);
        
        boolean zamiana = false;
        for(int i=0;i<N;i++){
            zamiana = false;
            for(int j=0;i<N-1 && zamiana;i++){
                if(tab[j]>tab[j+1]){
                    int pom=tab[j];
                    tab[j]=tab[j+1];
                    tab[j+1]=pom;
                    zamiana = true;
                }
            }
        }
    } 
}
